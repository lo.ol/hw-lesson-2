#include "Mitochondrion.h"

/*initializing Mitochondrion*/
void Mitochondrion::init()
{
	this->_glocuse_level = 0;
	this->_has_glocuse_receptor = false;
}

/*checks if glucose receptor is in protein linked list*/
void Mitochondrion::insert_glucose_receptor(const Protein & protein)
{
	int i = 0;
	bool flag = true;
	//the right order for the receptor
	int rightProtein[7] = { ALANINE,LEUCINE,GLYCINE,HISTIDINE,LEUCINE,PHENYLALANINE,AMINO_CHAIN_END };
	AminoAcidNode* curr = protein.get_first();
	AminoAcid temp = curr->get_data();
	while (curr || i == 7)
	{
		temp = curr->get_data();
		if (temp != rightProtein[i])
		{
			flag = false;
			i = 0; //if order is not right start checking again
		}
		else
		{
			i++;
		}
		curr = curr->get_next();
	}
	if (i == 7) //if all aminoasids in place
	{
		flag = true;
	}
	this->_has_glocuse_receptor = flag;
	delete curr;
}

/*function checks if ATP can be produced*/
bool Mitochondrion::produceATP(const int glocuse_unit) const
{
	bool atp = false;
	if (glocuse_unit >= 50 && _has_glocuse_receptor)
	{
		atp = true;
	}
	return atp;
}

//setters
void Mitochondrion::set_glocuse(const unsigned int glocuse_unit)
{
	this->_glocuse_level = glocuse_unit;
}

void Mitochondrion::set_has_glocuse_receptor(const bool has_glocuse_receptor)
{
	this->_has_glocuse_receptor = has_glocuse_receptor;
}

//getters
unsigned int Mitochondrion::get_glocuse_level() const
{
	return _glocuse_level;
}

bool Mitochondrion::get_has_glocuse_receptor() const
{
	return _has_glocuse_receptor;
}