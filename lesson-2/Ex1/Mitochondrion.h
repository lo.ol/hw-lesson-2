#pragma once
#include "Protein.h"
class Mitochondrion
{
	private:
		unsigned int _glocuse_level;
		bool _has_glocuse_receptor;

	public:

		void init();
		void insert_glucose_receptor(const Protein & protein);
		bool produceATP(const int glocuse_unit) const;

		//setters
		void set_glocuse(const unsigned int glocuse_unit);
		void set_has_glocuse_receptor(bool has_glocuse_receptor);

		//getters
		unsigned int get_glocuse_level() const;
		bool get_has_glocuse_receptor() const;
};