#include "Cell.h"

/*initializing cell with parameters*/
void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	this->_glocus_receptor_gene = glucose_receptor_gene;
	this->_nucleus.init(dna_sequence);
	this->_mitochondrion.init();
	this->_atp_units = 0;
}

/*function represents the cell
it gets rna script, put it in ribosome to create new protein
checks if protien contains the right glucose receptor
and sets glucose to 50
checks if atp can be produced, and returns result*/
bool Cell::get_ATP()
{
	std::string RNA = _nucleus.get_RNA_transcript(_glocus_receptor_gene);
	Protein* protein =_ribosome.create_protein(RNA);
	if (protein == nullptr) //in case protein cant be created
	{
		// writes to cerr - a stream dedicated to error audit
		std::cerr << "Protein not created, wrong parameters!" << std::endl;
		_exit(1);
	}
	_mitochondrion.insert_glucose_receptor(*protein);
	_mitochondrion.set_glocuse(50);
	if (_mitochondrion.produceATP(_mitochondrion.get_glocuse_level()))
	{
		_atp_units = 100;
		return true;
	}
	else
	{
		return false;
	}
}