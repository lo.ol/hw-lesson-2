#include "Ribosome.h"

Protein* Ribosome::create_protein(std::string &RNA_transcript) const
{
	std::string aminoAcid = "";
	AminoAcid toAdd;
	Protein* newProtein = new Protein;
	(*newProtein).init();
	while(RNA_transcript.length() >= 3)
	{
		aminoAcid = RNA_transcript.substr(0, 3);
		RNA_transcript.erase(0, 3);
		toAdd = get_amino_acid(aminoAcid);
		if (toAdd == UNKNOWN)
		{
			(*newProtein).clear();
			return nullptr;
		}
		else
		{
			(*newProtein).add(toAdd);
			aminoAcid = "";
		}
	}
	return newProtein; 
}