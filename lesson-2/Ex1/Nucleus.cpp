#include "Nucleus.h"

using std::endl;



/*function will initialize the Gene*/
void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	this->_start = start;
	this->_end = end;
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}

/*function will initialize nucleus.
check if dna_sequence is valid and
puts right match to it in complementary dna*/
void Nucleus::init(const std::string dna_sequence) 
{
	std::cout << dna_sequence << std::endl;
	for (int i = 0; i < dna_sequence.length(); i++)
	{
		if (dna_sequence[i] != 'A' && dna_sequence[i] != 'T' && dna_sequence[i] != 'C' && dna_sequence[i] != 'G')
		{
			// writes to cerr - a stream dedicated to error audit
			std::cerr << "In dna_sequence must be only 'A', 'T', 'G', 'C' letters" << endl;
			_exit(1);
		}
		else
		{
			this->_DNA_strand += dna_sequence[i];
			switch (dna_sequence[i]) //switch case to find right match
			{
			case 'A':
				this->_complementary_DNA_strand += 'T';
				break;
			case 'T':
				this->_complementary_DNA_strand += 'A';
				break;
			case 'C':
				this->_complementary_DNA_strand += 'G';
				break;
			case 'G':
				this->_complementary_DNA_strand += 'C';
				break;
			}
		}
	}
	 
}

/*function will use gene parameters to get rna script
end and start to get the right part of dna script
and _on_complementary_dna_strand to know from where to get script*/
std::string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	std::string RNA = "";
	int size = gene.get_end() - gene.get_start();
	std::string dna;
	if (gene.is_on_complementary_dna_strand())
	{
		dna = this->_complementary_DNA_strand;
	}
	else
	{
		dna = this->_DNA_strand;
	}
	if (gene.get_end() > dna.length()) 
	{
		// writes to cerr - a stream dedicated to error audit
		std::cerr << "Gene parameters wrong, end can't be bigget than dna strand length" << endl;
		_exit(1);
	}
	for (int i = gene.get_start(); i <= gene.get_end(); i++)
	{
		if (dna[i] == 'T') //changes T to U
		{
			RNA += 'U';
		}
		else
		{
			RNA += dna[i];
		}
	}
	
	
	return RNA;
}

/*function will reverse dna strand*/
std::string Nucleus::get_reversed_DNA_strand() const
{
	std::string dna = _DNA_strand;
	int len = dna.length();

	// Swap character starting from two 
	// corners 
	for (int i = 0; i < len / 2; i++)
	{
		std::swap(dna[i], dna[len - i - 1]);
	}
	return dna;
}

/*function will find how many times a certain codon appears in dna*/
unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	std::string dna = _DNA_strand;
	int i = 0, codonLen = codon.length(), count = 0;
	while (i != -1 &&  i < dna.length())
	{
		i = dna.find(codon, i);
		if (i != -1) //if a codon found
		{
			count++; 
			i += codonLen; //i the place in string where the codon starts, to prevent finding the same codon its len added.
		}
		
	}
	return count;
}

//setters
void Nucleus::set_DNA_strand(const std::string dna)
{
	for (int i = 0; i < dna.size(); i++)
	{
		if (dna[i] != 'A' && dna[i] != 'T' && dna[i] != 'C' && dna[i] != 'G')
		{
			// writes to cerr - a stream dedicated to error audit
			std::cerr << "In dna_sequence must be only 'A', 'T', 'G', 'C' letters" << endl;
			_exit(1);
		}
	}
	this->_DNA_strand = dna;
}

void Nucleus::set_complementary_DNA_strand(const std::string complementary_dna)
{
	
	for (unsigned int i = 0; i < complementary_dna.size(); i++)
	{

		switch (complementary_dna[i])
		{
			case 'A':
				this->_complementary_DNA_strand += 'T';
				break;
			case 'T':
				this->_complementary_DNA_strand += 'A';
				break;
			case 'C':
				this->_complementary_DNA_strand += 'G';
				break;
			case 'G':
				this->_complementary_DNA_strand += 'C';
				break;
			default:
				// writes to cerr - a stream dedicated to error audit
				std::cerr << "In dna_sequence must be only 'A', 'T', 'G', 'C' letters" << endl;
				_exit(1);
				break;
		}
	}
}

void Gene::set_start(const unsigned int start)
{
	this->_start = start;
}

void Gene::set_end(const unsigned int end)
{
	this->_end = end;
}

void Gene::set_complementary_dna_strand(const bool _on_complementary_dna_strand)
{
	this->_on_complementary_dna_strand = _on_complementary_dna_strand;
}

//getters
std::string Nucleus::get_DNA_strand() const
{
	return this->_DNA_strand;
}

std::string Nucleus::get_complementary_DNA_strand() const
{
	return this->_complementary_DNA_strand;
}

unsigned int Gene::get_start() const
{
	return this->_start;
}

unsigned int Gene::get_end() const
{
	return this->_end;
}

bool Gene::is_on_complementary_dna_strand() const
{
	return this->_on_complementary_dna_strand;
}